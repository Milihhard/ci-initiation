<?php
declare(strict_types=1);

/**
 * Todo Manager
 *
 * @package Nopackage
 * @author Emilien Nicolas <emilien.nicolas@passerellesnumeriques.com>
 */
class ToDoManager
{
    /**
     * Name of the task
     *
     * @var string
     */
    private $todos;

    /**
     * Construct this class
     */
    public function __construct()
    {
        $this->todos = [];
    }//end __construct()

    /**
     * Add a todo to the list
     * @param string $taskName task name
     * @return void
     */
    public function addToDoList(string $taskName): void
    {
        array_push($this->todos, new ToDo($taskName));
    }

    /**
     * Remove a task in the list
     * @param int $index index to delete
     * @return void
     */
    public function removeToDoList(int $index = 0): void
    {
        // FIXME: it's broken
        array_splice($this->todos, $index, 1);
    }

    /**
     * Clear the task list
     * @return void
     */
    public function clearToDoList(): void
    {
        unset($this->todos);
        $this->todos = [];
    }

    /**
     * Get a task of the list
     * @param  int  $index index to get
     * @return ToDo        Task
     */
    public function getToDo(int $index): ToDo
    {
        return $this->todos[$index];
    }

    /**
     * Get all tasks of the list
     * @return array        Tasks
     */
    public function getToDos(): array
    {
        return $this->todos;
    }

    /**
     * Get tasks length
     * @return int        length of tasks list
     */
    public function getToDosLength(): int
    {
        return count($this->todos);
    }
}//end class
