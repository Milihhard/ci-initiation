<?php
declare(strict_types=1);

/**
 * Todo Model
 *
 * @var class
 * @package Nopackage
 * @author Emilien Nicolas <emilien.nicolas@passerellesnumeriques.com>
 */
class ToDo
{
    private $taskName;

    /**
     * State of the todo
     * @var bool
     */
    private $checked;

    /**
     * Construct this class
     * @param string $taskName name of the task
     */
    public function __construct($taskName)
    {
        $this->taskName = $taskName;
        $this->checked = false;
    }//end __construct()

    /**
     * Get the name of the task
     * @return string task name
     */
    public function getTaskName(): string
    {
        return $this->taskName;
    }

    /**
     * Check if the task is checked
     * @return bool true if checked
     */
    public function isChecked(): bool
    {
        return $this->checked;
    }

    /**
     * set the name of the task
     * @param string $taskName task name
     * @return void
     */
    public function setTaskName(string $taskName): void
    {
        $this->taskName = $taskName;
    }

    /**
     * set check
     * @param bool $checked check state
     * @return void
     */
    public function setChecked(bool $checked): void
    {
        $this->checked = $checked;
    }

    /**
     * Set check to true
     * @return void
     */
    public function check(): void
    {
        $this->setChecked(true);
    }

    /**
     * Set check to false
     * @return void
     */
    public function unCheck(): void
    {
        $this->setChecked(false);
    }
}//end class
