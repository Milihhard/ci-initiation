<?php
require_once 'model/ToDo.php';
require_once 'model/ToDoManager.php';
$list = new ToDoManager();
$list->addToDoList('Do Homework');
$list->addToDoList('ToDo');
$list->getToDo(0)->check();
    ?>
    <h1>ToDo List</h1>
    <table>
        <tr>
            <th>Checked</th>
            <th>Name</th>
        </tr>
        <?php
        foreach ($list->getToDos() as $todo) {
            ?>
            <tr>
                <td> <input type="checkbox" name="" value="" <?php if ($todo->isChecked()) {
                echo 'checked';
            } ?>></td>
                <td><?php echo $todo->getTaskName() ?></td>
            </tr>
            <?php
        }
         ?>
    </table>
