<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

/**
 * Test for todo class
 *
 * @testdox Test for todo class
 * @package Nopackage
 * @author Emilien Nicolas <emilien.nicolas@passerellesnumeriques.com>
 */
final class ToDoTest extends TestCase
{

    /**
     * Todo test object
     * @var ToDo
     */
    private $todo;

    /**
     * Method called before each test function
     *
     * @return void
     */
    protected function setUp()
    {
        $this->todo = new ToDo('test');
    }//end setUp()


    /**
     * Method called after each test function
     *
     * @return void
     */
    protected function tearDown()
    {
        unset($this->todo);
    }//end tearDown()

    /**
     * The todo should be initialized
     * @return void
     * @test
     * @testdox The todo should be initialized
     */
    public function shouldBeInitialized()
    {
        $this->assertEquals('test', $this->todo->getTaskName());
    }

    /**
     * The todo should be unchecked at initiation
     * @return void
     * @test
     * @testdox The todo should be unchecked at initiation
     */
    public function ShouldNotBeCheckedInit()
    {
        $this->assertFalse($this->todo->isChecked());
    }

    /**
     * The todo should be checked
     * @return void
     * @test
     * @testdox The todo should be checked
     */
    public function ShouldBeChecked()
    {
        $this->todo->check();
        $this->assertTrue($this->todo->isChecked());
    }

    /**
     * The todo should be unchecked
     * @test
     * @testdox The todo should be unchecked
     */
    public function ShouldNotBeChecked()
    {
        $this->todo->unCheck();
        $this->assertFalse($this->todo->isChecked());
    }

    /**
     * The todo should have another name
     * @return void
     * @test
     * @testdox The todo should have another name
     */
    public function ShouldChangeName()
    {
        $this->todo->setTaskName('New name');
        $this->assertEquals('New name', $this->todo->getTaskName());
    }
}//end class
