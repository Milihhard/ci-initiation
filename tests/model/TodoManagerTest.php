<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

/**
 * Test for todo manager class
 *
 * @testdox Test for todo manager class
 * @package Nopackage
 * @author Emilien Nicolas <emilien.nicolas@passerellesnumeriques.com>
 */
final class ToDoManagerTest extends TestCase
{

    /**
     * Todo list test object
     * @var ToDoManager
     */
    private $todos;

    /**
     * Method called before each test function
     *
     * @return void
     */
    protected function setUp()
    {
        $this->todos = new ToDoManager();
    }//end setUp()


    /**
     * Method called after each test function
     *
     * @return void
     */
    protected function tearDown()
    {
        unset($this->todos);
    }//end tearDown()

    /**
     * The todos should be initialized
     * @return void
     * @test
     * @testdox The todos should be initialized
     */
    public function shouldBeInitialized()
    {
        $this->assertEquals(0, $this->todos->getToDosLength());
    }

    /**
     * Adding a task in the list
     * @return void
     * @test
     * @testdox Adding a task in the list
     */
    public function shouldAddATask()
    {
        $before = $this->todos->getToDosLength();
        $this->todos->addToDoList('Test');
        $this->assertEquals($before + 1, $this->todos->getToDosLength());
        $this->assertFalse($this->todos->getToDo(0)->isChecked());
        return $this->todos;
    }

    /**
     * Removing a task in the list
     * @param object $tasks tasks
     * @return void
     * @test
     * @testdox Removing a task in the list
     * @depends clone shouldAddATask
     */
    public function shouldRemoveATask(object $tasks)
    {
        $before = $tasks->getToDosLength();
        $tasks->removeToDoList();
        $this->assertEquals($before - 1, $tasks->getToDosLength());
    }

    /**
     * Removing a specific task in the list
     * @param object $tasks tasks
     * @return void
     * @test
     * @testdox Removing a specific task in the list
     * @depends clone shouldAddATask
     */
    public function shouldRemoveASpecificTask(object $tasks)
    {
        $tasks->addToDoList('Test2');
        $before = $tasks->getToDosLength();
        $tasks->removeToDoList(1);
        $this->assertEquals($before - 1, $tasks->getToDosLength());
        $this->assertEquals('Test', $tasks->getToDo(0)->getTaskName());
    }

    /**
     * Removing an undefined task in the list
     * @return void
     * @test
     * @testdox Removing an undefined task in the list
     * @expectedException LogicException
     */
    public function shouldNotRemoveASpecificTask()
    {
        $this->todos->removeToDoList();
    }

    /**
     * Removing an undefined index task in the list
     * @param object $tasks tasks
     * @return void
     * @test
     * @testdox Removing an undefined index task in the list
     * @expectedException LogicException
     * @depends clone shouldAddATask
     */
    public function shouldNotRemoveASpecificTaskIndex(object $tasks)
    {
        $tasks->addToDoList('Test');
        $tasks->removeToDoList(3);
    }

    /**
     * Clear the list
     * @param object $tasks tasks
     * @return void
     * @test
     * @testdox Clear the list
     * @depends clone shouldAddATask
     */
    public function clearList(object $tasks)
    {
        $tasks->clearToDoList();
        $this->assertEquals(0, $tasks->getToDosLength());
    }

    /**
     * Get a task from the list
     * @param object $tasks tasks
     * @return void
     * @test
     * @testdox Get a task from the list
     * @depends clone shouldAddATask
     */
    public function getTodo(object $tasks)
    {
        $todo = $tasks->getToDo(0);
        $this->assertEquals("Test", $todo->getTaskName());
    }

    /**
     * Get an undefined task from the list
     * @param object $tasks tasks
     * @return void
     * @test
     * @testdox Get an undefined task from the list
     * @expectedException LogicException
     * @depends clone shouldAddATask
     */
    public function getTodoUndefined(object $tasks)
    {
        $todo = $tasks->getToDo(2);
    }

    /**
     * Get the tasks list
     * @param object $tasks tasks
     * @return void
     * @test
     * @testdox Get the tasks list
     * @depends clone shouldAddATask
     */
    public function getList(object $tasks)
    {
        $tasks->addToDoList('Test2');
        $list = $tasks->getToDos();
        $this->assertEquals(2, count($list));
    }
}//end class
